<?php

class User extends CActiveRecord
{
	
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $profile
	 */
const LEVEL_REGISTERED=0, LEVEL_CONTACTO = 10, LEVEL_COORDINADOR = 55, LEVEL_EJECUTIVO = 50, LEVEL_ADMIN=99,  LEVEL_ROOT=100, LEVEL_OPERADOR = 75;
 
 /**
  * define the label for each level
  * @param int $level the level to get the label or null to return a list of labels
  * @return array|string
  */


public function getaccess($lv)
{
    if($lv == 'Root') {
        return 100; 
    } elseif($switch == 'Administrador del sistema') {
        return 99; 
    }
     elseif($switch == 'Operador (Telefonista)') {
        return 75; 
    }
	 elseif($switch == 'Coordinador General') {
        return 55; 
    }
     elseif($switch == 'Ejecutivo') {
        return 50; 
    }
     elseif($switch == 'Contacto') {
        return 10; 
    }
     else {
        return 0; 
    }
}


 static function getAccessLevelList( $level = null ){
  	
	 $levelList=array(

	self::LEVEL_ROOT => 'Root',
	self::LEVEL_ADMIN => 'Administrador del sistema',
	self::LEVEL_OPERADOR => 'Operador (Telefonista)',
	self::LEVEL_COORDINADOR => 'Coordinador General',
	self::LEVEL_EJECUTIVO => 'Ejecutivo',
	self::LEVEL_CONTACTO => 'Contacto'
  );

  	if(!Yii::app()->user->getisRoot())
		unset($levelList[100]);
  	if(!Yii::app()->user->getisAdmin()){
		unset($levelList[99]);
			
	}
  	if(!Yii::app()->user->getisOperador()){
		unset($levelList[75]);
		unset($levelList[55]);
	}
	if(!Yii::app()->user->getisCoordgen())
		unset($levelList[55]);	
	if(!Yii::app()->user->getisEjecutivo())
		unset($levelList[50]);
	if(!Yii::app()->user->getisContacto())
		unset($levelList[10]);
	
  if( $level === null)
   return $levelList;
  return $levelList[ $level ];
 }
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$accs = 'user_id, firstname, lastname, username, password';

		return array(
			//array('image', 'file', 'types'=>'jpg, gif, png'),
			array($accs, 'required'),
				
			array('user_id, status, accessLevel', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname, username, password,  email, direccion, salt', 'length', 'max'=>128),
			array('user_id, firstname, lastname, username, direccion, salt, email, id_empresa, date_added, status, accessLevel', 'safe'),


		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'posts' => array(self::HAS_MANY, 'Post', 'author_id'),
			'accessLevel' => array(self::BELONGS_TO, 'User', 'accessLevel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
			'user_id' => 'Id',
			'username' => 'Nombre de Usuario',
			'firstname' => 'Nombre(s)',
			'lastname' => 'Apellido(s)',
			'password' => 'Password',
			'date_added' => 'Fecha de ingreso',
			'id_empresa' => 'Empresa',
			'status' => 'Estado',
			'accessLevel' => 'Nivel de acceso',
			
		);
	}
public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('username',$this->username,true);
		
		$criteria->compare('email',$this->email,true);
		
		$criteria->compare('id_empresa',$this->id_empresa);
		
		$criteria->compare('date_added',$this->date_added,true);

		$criteria->compare('status',$this->status);
		
		
		if((!Yii::app()->user->getIsEjecutivo()) && (Yii::app()->user->getisContacto())){
			$criteria->compare('accessLevel', User::model()->findByPk(Yii::app()->user->id_user)->accessLevel);
		}

		if((Yii::app()->user->getIsEjecutivo()) && (!Yii::app()->user->getisCoordgen())){
			$criteria->compare('accessLevel  <', User::model()->findByPk(Yii::app()->user->id_user)->accessLevel);
		}		
		if((Yii::app()->user->getisCoordgen()) && (!Yii::app()->user->getisOperador())){
			$criteria->compare('accessLevel  <', User::model()->findByPk(Yii::app()->user->id_user)->accessLevel);
		}
		if((Yii::app()->user->getisOperador()) && (!Yii::app()->user->getisAdmin())){
			$criteria->compare('accessLevel  <', User::model()->findByPk(Yii::app()->user->id_user)->accessLevel);
		}
		
		if((!Yii::app()->user->getIsRoot()) && (Yii::app()->user->getisAdmin())){	
			$criteria->compare('accessLevel  <', User::model()->findByPk(Yii::app()->user->id_user)->accessLevel);
		}
		
		return new CActiveDataProvider(get_class($this),array(
    'pagination'=>array(
        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    ),
    'criteria'=>$criteria,
));
		//return new CActiveDataProvider('User', array(
		//	'criteria'=>$criteria,
		//));
	}
	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	
	
	
	public function validatePassword($password)
	{

		if($this->status == 1){
			if($this->password === sha1($this->salt.sha1($this->salt.sha1($password))))
				return true;
			if($this->password === md5($password))
				return true;
			else{
				echo md5($password);
				return false;
				}
			}
		else
			return false;
		//"password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') "
		//return CPasswordHelper::verifyPassword($password,$this->password);
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return md5($password);
		//return CPasswordHelper::hashPassword($password);
	}
}
