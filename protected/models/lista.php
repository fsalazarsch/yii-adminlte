<?php

class lista extends RActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
    {
        return self::getAdvertDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{lista_contacto}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_lista, nombre, id_empresa, id_usuario', 'required'),
			array('nombre, fecha_creacion, ultima_actualizacion', 'length', 'max'=>128),
			array('id_lista, id_empresa, id_usuario, borrado', 'numerical', 'integerOnly'=>true),
			array('id_lista, nombre, fecha_creacion, ultima_actualizacion, id_empresa, id_usuario, borrado', 'safe'),
			array('id_lista, nombre, fecha_creacion, ultima_actualizacion, id_empresa, id_usuario, borrado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_empresa' => 'Empresa',
			'id_usuario' => 'Usuario',			
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('nombre',$this->nombre, true);

		$criteria->compare('fecha_creacion',$this->fecha_creacion, true);

		$criteria->compare('ultima_actualizacion',$this->ultima_actualizacion, true);

		$criteria->compare('id_empresa',$this->id_empresa);

		$criteria->compare('id_usuario',$this->id_usuario);

		$criteria->compare('borrado',$this->borrado);
		
		return new CActiveDataProvider('lista', array(
			'criteria'=>$criteria,
		));
	}


}
