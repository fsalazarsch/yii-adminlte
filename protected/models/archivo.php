<?php

class archivo extends RActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
    {
        return self::getAdvertDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{archivos}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(		
			array('id_archivo, nombre, ruta, creacion, tipo_archivo, tipo', 'required'),
			array('nombre, ruta, creacion, tipo_archivo', 'length', 'max'=>128),
			array('id_archivo, tipo', 'numerical', 'integerOnly'=>true),
			array('id_archivo, nombre, ruta, creacion, tipo_archivo, tipo', 'safe'),
			array('id_archivo, nombre, ruta, creacion, tipo_archivo, tipo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_archivo' => 'Id',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
	
		$criteria->compare('nombre',$this->nombre, true);

		$criteria->compare('creacion',$this->creacion, true);

		$criteria->compare('tipo_archivo',$this->tipo_archivo, true);
		
		return new CActiveDataProvider('archivo', array(
			'criteria'=>$criteria,
		));
	}


}
