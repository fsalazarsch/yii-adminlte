<?php

class cliente extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, customer_group_id, rut_empresa, razon_social, email, telephone', 'required'),
			array('rut_empresa, razon_social, date_added', 'length', 'max'=>128),
			array('customer_id, customer_group_id, telephone', 'numerical', 'integerOnly'=>true),
			array('customer_id, customer_group_id, razon_social, email, date_added, telephone, rut_empresa', 'safe'),
			array('customer_id, customer_group_id, razon_social, email, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'customer_id' => 'Id Empresa',
			'razon_social' => 'Razón Social',
			'rut_empresa' => 'Rut Cliente',
			'customer_group_id' => 'Grupo de Cliente',
			'date_added' => 'Fecha de inscripcion',
			'email' => 'Email',
			'telephone' => 'Telefono',
			
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('customer_id',$this->customer_id);

		$criteria->compare('razon_social',$this->razon_social, true);

		$criteria->compare('customer_group_id',$this->customer_group_id);

		$criteria->compare('date_added',$this->date_added, true);

		$criteria->compare('email',$this->email, true);
		
		return new CActiveDataProvider('cliente', array(
			'criteria'=>$criteria,
		));
	}


}
