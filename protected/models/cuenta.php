<?php

class cuenta extends RActiveRecord
{
	
	public function getDbConnection()
    {
        return self::getAdvertDbConnection();
    }


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cuenta_corriente}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cuenta_corriente, saldo_credito, id_empresa, saldo_mail, saldo_sms, consumidos_mail, consumidos_sms, id_conector_sms, id_conector_mail', 'required'),
			array('id_cuenta_corriente, saldo_credito, id_empresa, saldo_mail, saldo_sms, consumidos_mail, consumidos_sms, id_conector_sms, id_conector_mail', 'numerical', 'integerOnly'=>true),
			array('id_cuenta_corriente, saldo_credito, id_empresa, saldo_mail, saldo_sms, consumidos_mail, consumidos_sms, id_conector_sms, id_conector_mail', 'safe'),
			array('id_cuenta_corriente, saldo_credito, id_empresa, saldo_mail, saldo_sms, consumidos_mail, consumidos_sms, id_conector_sms, id_conector_mail', 'safe', 'on'=> 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'id_empresa' => array(self::BELONGS_TO, 'cliente', 'id_empresa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cuenta_corriente' => 'Id',
			'saldo_credito' => 'Saldo Credito',
			'id_empresa' => 'Empresa',
			'saldo_mail' => 'Saldo mail',
			'saldo_sms' => 'Saldo SMS',
			'consumidos_mail' => 'Mail Consumidos',
			'consumidos_sms' => 'SMS Consumidos',
			'id_conector_sms' => 'Conector SMS',
			'id_conector_mail' => 'Conector Mail',

		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		
		$criteria->compare('id_cuenta_corriente',$this->id_cuenta_corriente);
		$criteria->compare('saldo_credito',$this->saldo_credito);
		$criteria->compare('id_empresa',$this->id_empresa);
		$criteria->compare('saldo_mail',$this->saldo_mail);
		$criteria->compare('saldo_sms',$this->saldo_sms);
		$criteria->compare('consumidos_mail',$this->consumidos_mail);
		$criteria->compare('consumidos_sms',$this->consumidos_sms);
		$criteria->compare('id_conector_sms',$this->id_conector_sms);
		$criteria->compare('id_conector_mail',$this->id_conector_mail);

		
		return new CActiveDataProvider('cuenta', array(
			'criteria'=>$criteria,
		));
	}


}
