<?php

class compra extends  extends RActiveRecord
{
	public function getDbConnection()
    {
        return self::getAdvertDbConnection();
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{compra}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_compra, cantidad_sms, cantidad_mail, fecha, valor, id_cuenta_corriente, cantidad_creditos', 'required'),
			array('fecha', 'length', 'max'=>128),
			array('id_compra, cantidad_sms, cantidad_mail, valor, id_cuenta_corriente, cantidad_creditos', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_compra' => 'Id',
			'cantidad_sms' => 'Cantidad Sms',
			'cantidad_mail' => 'Cantidad Mail',
			'cantidad_creditos' => 'Creditos',
			'id_cuenta_corriente' => 'Cuenta Corriente',
			
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_compra',$this->id_compra);

		$criteria->compare('cantidad_creditos',$this->cantidad_creditos);

		$criteria->compare('id_cuenta_corriente',$this->id_cuenta_corriente);
		
		return new CActiveDataProvider('compra', array(
			'criteria'=>$criteria,
			//'sort'=>array(
			//	'defaultOrder'=>'cantidad_creditos DESC',
			//),
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}
