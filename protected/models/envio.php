<?php

class envio extends RActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
    {
        return self::getAdvertDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{envio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(		
			array('id_envio, estado, remitente, id_mensaje, tipo_envio, tipo_mensaje, cuando_enviar, nombre_envio,  id_usuario, id_empresa', 'required'),
			array('remitente, correo_remitente, tipo_envio, tipo_mensaje, cuando_enviar, nombre_envio, datos_envio_programado', 'length', 'max'=>128),
			array('id_envio, estado, id_lista, id_mensaje, id_empresa, id_conector, id_consumo, correos_malos, id_usuario', 'numerical', 'integerOnly'=>true),
			array('id_envio, estado, remitente, correo_remitente, id_lista, id_mensaje, tipo_envio, tipo_mensaje, cuando_enviar, nombre_envio, correos_malos, id_usuario, datos_envio_programado, id_empresa, id_conector, id_consumo', 'safe'),
			array('id_envio, estado, remitente, correo_remitente, id_lista, id_mensaje, tipo_envio, tipo_mensaje, cuando_enviar, nombre_envio, correos_malos, id_usuario, datos_envio_programado, id_empresa, id_conector, id_consumo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_lista' => 'Lista',
			 'id_mensaje' => 'Mensaje',
			 'id_empresa' => 'Empresa',
			 'id_conector' => 'Conector',
			 'id_consumo' => 'Consumo',
			'id_usuario' => 'Usuario',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
	
		$criteria->compare('estado',$this->estado, true);

		$criteria->compare('tipo_mensaje',$this->tipo_mensaje, true);

		$criteria->compare('tipo_envio',$this->tipo_envio, true);

		$criteria->compare('id_lista',$this->id_lista);

		$criteria->compare('id_empresa',$this->id_empresa);

		$criteria->compare('id_usuario',$this->id_usuario);

		$criteria->compare('id_conector',$this->id_conector);
		
		return new CActiveDataProvider('envio', array(
			'criteria'=>$criteria,
		));
	}


}
