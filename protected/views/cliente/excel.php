<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("cityexpress")
    ->setLastModifiedBy("cityexpress");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Usuarios');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID_USER');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'EMAIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'CARGO');
				
				
				$sql ='';
				
				
				if( User::model()->findByPk(Yii::app()->user->id)->accessLevel == 99){
					$sql .=' where accessLevel <= '.User::model()->findByPk(Yii::app()->user->id)->accessLevel;
				}

				if( User::model()->findByPk(Yii::app()->user->id)->accessLevel == 75){
					$sql .=' where accessLevel <= '.User::model()->findByPk(Yii::app()->user->id)->accessLevel;
				}				
				
				if( User::model()->findByPk(Yii::app()->user->id)->accessLevel <= 50){
					$sql .=' where programa = '.User::model()->findByPk(Yii::app()->user->id)->programa;
				}
				
				$proveedores = Yii::app()->db->createCommand('SELECT * FROM tbl_user'.$sql)->queryAll();
				
				$i=0;
				foreach($proveedores as $p){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $p['id']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $p['username']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $p['email']);
				
				if( $p['accessLevel'] == 10)
					$ac = 'Contacto';
				if( $p['accessLevel'] == 100)
					$ac = 'Root';
				if( $p['accessLevel'] == 99)
					$ac = 'Administrador';
				if( $p['accessLevel'] == 75)
					$ac = 'Telefonista';
				if( $p['accessLevel'] == 50)
					$ac = 'Ejecutivo';
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $ac);
				
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="usuarios.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>