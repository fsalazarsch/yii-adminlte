<?
function get_cuenta($empresa){
	return Yii::app()->db->createCommand('Select id_cuenta_corriente FROM assert_cuenta_corriente where id_empresa = '.$empresa)->queryScalar();
	}
function parsedate($fecha){
	return date('d-m-Y', strtotime($fecha));
	}
?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar cliente</h2>
<div class="pull-right">
<a href="<? echo Yii::app()->createUrl("cliente/create"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<!--a class="btn btn-success"><i class="fa fa-file-excel-o"></i></a-->
</div>

</div>

<?
$this->breadcrumbs=array(
	'cliente'=>array('index'),
	'Administrar',
);
?>
<!--a class="btn btn-default" data-toggle="tooltip" title="Mis Listas" data-original-title="Mis Listas"><i class="fa fa-mail-reply"></i></a-->
<?

Yii::app()->clientScript->registerScript('search', "


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
",  CClientScript::POS_LOAD);
?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cliente-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
	'customer_id',
	'razon_social',
	array(
	'name' => 'customer_group_id',
    'value'=> 'grupo::model()->findbyPk($data->customer_group_id)->nomdesc',
	),
	
		array(
		'name' => 'date_added',
            'value'=> 'parsedate($data->date_added)',
		),
	'email',
		
		array(
    'class'=>'CButtonColumn',
    			'template' => '{editar_driver} {editar_creditos} {editar} {login} {borrar}',
    			'htmlOptions' => array('style' => 'width: 220px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(
				'editar_driver' => array
				(
					'label'=>'<button class="btn btn-warning"><i class="fa fa-gears"></i></button>',
					 'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("cuenta/updateconector", array("id"=>get_cuenta($data->customer_id)))',
				
				),

				'editar_creditos' => array
				(
					'label'=>'<button class="btn btn-flat"><i class="fa fa-usd"></i></button>',
					 'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("cuenta/updatesaldo", array("id"=>get_cuenta($data->customer_id)))',
				
				),

				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("cliente/update", array("id"=>$data->customer_id))',
						
				),	
				'login' => array
				(
					'label'=>'<button class="btn btn-success"><i class="fa fa-home"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("cliente/view", array("id"=>$data->customer_id))',
					
				),
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("cliente/update", array("id"=>$data->customer_id))',
					
				),
				),
    ),
	),
)); ?>
<br><br>

