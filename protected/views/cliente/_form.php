

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data', 'role'=> 'form'),
)); ?>

<div class="box box-primary box-header with-border">
<? if(!$model->isNewRecord){ ?>	
<h2 class="box-title">Administrar cliente</h2>
<?}
else{
?>
<h2 class="box-title">Registrar cliente</h2>
<?}?>

<div class="pull-right">
	<div class="row box-footer">
		<?echo CHtml::tag('button',
                array('class' => 'btn btn-large pull-right btn btn-primary', 'onclick'=> '"this.form.submit()"'),
                '<i class="fa fa-save"></i>');
                ?>
	</div>
</div>

</div>


<div class="box-body">
	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'razon_social'); ?>
		<?php echo $form->textField($model,'razon_social',array('class'=>'form-control','maxlength'=>128)); ?>
		<?php echo $form->error($model,'razon_social', array('class'=>'help-block')); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'rut_empresa'); ?>
		<?php echo $form->textField($model,'rut_empresa',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'rut_empresa', array('class'=>'help-block')); ?>
	</div>	
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email', array('class'=>'help-block')); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->numberField($model,'telephone',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'telephone', array('class'=>'help-block')); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'customer_group_id'); ?>
		<?php echo $form->dropDownList($model,'customer_group_id',CHtml::listData(grupo::model()->findAll(array('order'=>'name')), 'customer_group_id','nomdesc'), array('empty'=>'Seleccionar..', 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'customer_group_id', array('class'=>'help-block')); ?>
	</div>

		
	<!--div class="row box-footer">
		< ?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar', array('class' => 'btn btn-primary')); ?>
	</div-->

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
