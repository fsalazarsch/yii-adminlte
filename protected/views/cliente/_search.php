<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--div class="form-group">
		< ?php echo $form->label($model,'customer_id'); ?>
		< ?php echo $form->numberField($model,'customer_id',array('size'=>60,'maxlength'=>128)); ?>
	</div-->
<table>
	<tr>
	<td>
	<div class="form-group">
		<?php echo $form->label($model,'customer_group_id'); ?>
		<?php echo $form->dropDownList($model,'customer_group_id',CHtml::listData(grupo::model()->findAll(array('order'=>'name')), 'customer_group_id','nomdesc'), array('empty'=>'Seleccionar..', 'class' => 'form-control', 'style' =>'width: 250px')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'razon_social'); ?><br>
		<?php echo $form->textField($model,'razon_social',array('size'=>60,'maxlength'=>128)); ?>
	</div>
	</td>
	<td style="padding-left: 20px;">
	<div class="form-group">
		<?php echo $form->label($model,'email'); ?><br>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'date_added'); ?><br>
		<?php echo $form->dateField($model,'date_added',array('size'=>60,'maxlength'=>128)); ?>
	</div>
	</td>
	</tr>
	</table>
	<div class="form-group"> 
		<?php echo CHtml::submitButton('Buscar',  array('class' => 'btn btn-primary')); ?>
		
		
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
