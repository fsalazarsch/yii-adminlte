<? $baseUrldist = Yii::app()->theme->baseUrl?>
<section class="content-header">
      <h1>
        Dashboard
        <small>Panel de Control <?echo $model->razon_social?></small>
      </h1>
      <!--ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol-->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
				<p>
				<?php //echo Yii::app()->db2->createCommand('Select count(*) from compra where (cantidad_sms > 0 and cantidad_mail > 0)')->queryScalar();?>
				Consumidos/Disponibles<h3> <?php echo cuenta::model()->findByPk($model->customer_id)->consumidos_sms;?> /
				<?php echo cuenta::model()->findByPk($model->customer_id)->saldo_sms;?></h3>
				</p>

              <p>TOTAL DE SMS</p>
            </div>
            <div class="icon">
              <i class="fa fa-comments"></i>
            </div>
             <a href="#" class="small-box-footer"><i class="fa fa-shopping-cart"></i>Comprar más créditos ... </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
				<p>
				<?php //echo Yii::app()->db2->createCommand('Select count(*) from compra where (cantidad_sms > 0 and cantidad_mail > 0)')->queryScalar();?>
				Consumidos/Disponibles<h3> <?php echo cuenta::model()->findByPk($model->customer_id)->consumidos_mail;?> /
				<?php echo cuenta::model()->findByPk($model->customer_id)->saldo_mail;?></h3>
				</p>

              <p>EMAIL ENVIADOS</p>
            </div>
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-shopping-cart"></i>Comprar más créditos ... </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
				<p>
				Listas/Contactos<h3> <?php echo Yii::app()->db2->createCommand('Select count(*) from lista_contacto where id_empresa ='.$model->customer_id)->queryScalar();
				//echo cuenta::model()->findByPk($model->customer_id)->consumidos_mail;?> /
				<? echo Yii::app()->db2->createCommand('Select count(*) from contacto where id_lista in (Select id_lista from lista_contacto where id_empresa ='.$model->customer_id.')')->queryScalar();?>
				<?php //echo cuenta::model()->findByPk($model->customer_id)->saldo_mail;?></h3>
				</p>


              <p>Contactos</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i>Más información</a>
          </div>
        </div>
        <!-- ./col -->
      <!-- /.row -->
      <!-- Main row -->


 <!-- Content Wrapper. Contains page content -->
      <h3>
        Enviados
      </h3>
    </section>
<div id="legend"></div>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
        <div class="col-md-12">


          <div class="box box-success">
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
      </div>
 
 

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
'customer_id',
	'razon_social',
	array(
	'name' => 'customer_group_id',
    'value'=> 'grupo::model()->findbyPk($data->customer_group_id)->name',
	),
	
		array(
		'name' => 'date_added',
            'value'=> 'parsedate($data->date_added)',
		),
	'email',
		

	),
)); 
?>
<br><br>

<?
	
	//$sms = Yii::app()->db2->createCommand('SELECT DATE(cuando_enviar) as fecha, COUNT(*) as nro FROM envio WHERE MONTH(cuando_enviar) = 5 AND id_empresa ='.$model->customer_id.' AND tipo_mensaje = "SMS" GROUP BY DATE(cuando_enviar)')->queryAll();
	//print_r($sms);
	?>
    

<script src="<?echo $baseUrldist?>/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="<?echo $baseUrldist?>/plugins/chartjs/Chart.min.js"></script>

<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------
    
	var ejex = new Array(); 
	var d = new Date();
	for (var i=0; i< 30;i++){
		ejex[i] = (i+1).toString()+'-'+d.getMonth()+'-'+d.getFullYear();
		}
	
	//select count(*) from envio where date(cuando_enviar) = {{fecha}} and id_empresa = $model->customer_id and tipo_mensaje= 'SMS' 		
	var areaChartData = {
      labels: ejex,
      datasets: [
        {
          label: "Email",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: "SMS",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    };

    //-------------
    //- BAR CHART -
    //-------------
    
    	
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      scaleBeginAtZero: true,
      scaleShowGridLines: true,
      scaleGridLineColor: "rgba(0,0,0,.05)",
      scaleGridLineWidth: 1,
      scaleShowHorizontalLines: true,
      scaleShowVerticalLines: true,
      barShowStroke: true,
      barStrokeWidth: 2,
      barValueSpacing: 5,
      barDatasetSpacing: 1,
	  legendTemplate: "<ul class=\"<%=label.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);

    var legend =  "<div class=\"legend\" style='direction:ltr'>";
    for (var i=0; i<areaChartData.datasets.length; i++){
		legend += '<span style=\"background-color: '+areaChartData.datasets[i].pointColor+'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;'+areaChartData.datasets[i].label+'</br>';
			}
		legend += "</div>";
    	document.getElementById('legend').innerHTML = legend;
	
  });

  
</script>
