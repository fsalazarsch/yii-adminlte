
   <body class="page page-template-default custom-background waves-pagebuilder menu-fixed header-logo_left theme-full">

		<div id="theme-layout">

            <?php //include_once 'include/header.php'; ?>
            <!--Start Central Slider-->
<div id="slider" class="one-pageslider">
    <div id="rev_slider_10_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#eeeeee;padding:0px;margin-top:0px;margin-bottom:0px;max-height:650px;">
        <!-- START REVOLUTION SLIDER 4.6.5 fullwidth mode -->
        <div id="rev_slider_10_1" class="rev_slider fullwidthabanner" style="display:none;max-height:650px;height:650px;">
            <ul>
                <!--Start Slide 01-->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" data-saveperformance="off">
                <!-- MAIN IMAGE -->
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Slider-Home-01.jpg" alt="Slider1" data-bgposition="left top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <!--<div class="tp-caption TW_Home3 skewfromrightshort customout" data-x="center" data-hoffset="-1" data-y="center" data-voffset="-143" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="800" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
                    <strong>CONECTATE CON<br>
                    TUS CLIENTES</strong>
                </div>-->
                <!-- LAYER NR. 2 -->
                <!--<div class="tp-caption mediumlarge_light_white_center_rale sft" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-captionhidden="on" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
                    Creative is a template with infinte editing, and it can be used <br>
                     for variety of web sites, check our Ready Made Layouts
                </div>-->
                <!-- LAYER NR. 3 -->
                <div class="tp-caption TW_Home3 lfl tp-resizeme" data-x="center" data-hoffset="-3" data-y="center" data-voffset="255" data-speed="600" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">
                    <a href="/registro" target="_blank" class="btn btn-flat btn-l white-button" style="border-color:#fba61a;background-color:#fba61a; border-radius: 6px;"><span class="BtnSlider">empieza ahora</span></a>
                </div>
                </li>
                <!--End Slide 01-->
                <!--Start Slide 02-->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" data-saveperformance="off">
                <!-- MAIN IMAGE -->
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Slider-Home-02.jpg" alt="Slider2" data-bgposition="left top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <!--<div class="tp-caption TW_Home3 skewfromrightshort customout" data-x="center" data-hoffset="-1" data-y="center" data-voffset="-143" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="800" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
                    <strong>CONECTATE CON<br>
                    TUS CLIENTES</strong>
                </div>-->
                <!-- LAYER NR. 2 -->
                <!--<div class="tp-caption mediumlarge_light_white_center_rale sft" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-captionhidden="on" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
                    Creative is a template with infinte editing, and it can be used <br>
                     for variety of web sites, check our Ready Made Layouts
                </div>-->
                <!-- LAYER NR. 3 -->
                <div class="tp-caption TW_Home3 lfl tp-resizeme" data-x="center" data-hoffset="-3" data-y="center" data-voffset="255" data-speed="600" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">
                    <a href="/registro" target="_blank" class="btn btn-flat btn-l white-button" style="border-color:#fba61a;background-color:#fba61a; border-radius: 6px;"><span class="BtnSlider">empieza ahora</span></a>
                </div>
                </li>
                <!--End Slide 02-->
                <!--Star Slide 03-->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" data-saveperformance="off">
                <!-- MAIN IMAGE -->
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Slider-Home-03.jpg" alt="Slider3" data-bgposition="left top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <!--<div class="tp-caption TW_Home3_black skewfromrightshort customout" data-x="72" data-y="197" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="800" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
                    <strong>AGENCY</strong> LAYOUT
                </div>-->
                <!-- LAYER NR. 2 -->
                <!--<div class="tp-caption Themewaves_slider_text customin customout" data-x="70" data-y="304" data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
                    This Layout is made specially for agencies and different<br>
                     company, it contains all the elements needed to build your website
                </div>-->
                <!-- LAYER NR. 3 -->
                <div class="tp-caption TW_Home3 lfl tp-resizeme" data-x="center" data-hoffset="-3" data-y="center" data-voffset="255" data-speed="600" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">
                    <a href="<?php echo Yii::app()->basePath; ?>/registro" target="_blank" class="btn btn-flat btn-l white-button" style="border-color:#fba61a;background-color:#fba61a; border-radius: 6px;"><span class="BtnSlider">empieza ahora</span></a>
                </div>
                </li>
                <!--End Slide 03-->
                <!--Start Slide 04-->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" data-saveperformance="off">
                <!-- MAIN IMAGE -->
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/Slider-Home-04.jpg" alt="Slider4" data-bgposition="left top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <!--<div class="tp-caption themewaves_Slider_W skewfromrightshort customout" data-x="47" data-y="179" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="800" data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
                    <strong>CREATIVE</strong> LAYOUT + VIDEO BACKGROUND
                </div>-->
                <!-- LAYER NR. 2 -->
                <!--<div class="tp-caption mediumlarge_light_white_N customin customout" data-x="57" data-y="254" data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
                    This Layout is made specially for creatives and it is<br>
                     combinied with video background.
                </div>-->
                <!-- LAYER NR. 3 -->
                <div class="tp-caption TW_Home3 lfl tp-resizeme" data-x="center" data-hoffset="-3" data-y="center" data-voffset="255" data-speed="600" data-start="1300" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">
                    <a href="/registro" target="_blank" class="btn btn-flat btn-l white-button" style="border-color:#fba61a;background-color:#fba61a; border-radius: 6px;"><span class="BtnSlider">empieza ahora</span></a>
                </div>
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom">
            </div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>
<!--End Central Slider-->

			
            <!-- Start Main -->
			<section id="main">
			<div id="page">
            	<!--Start Content Connectus-->
                <!--Start New Services Responsive-->
                <div id="MainServices">
                	<div class="col-md-12">
                    	<div class="row">
                            <div class="col-md-12">
                				<div class="row">
            						<div id="MainTitle">
                                    	NUESTROS SERVICIOS | <span class="MainSubtitle">Envíos masivos de SMS y E-mail</span>
                                    </div>
                    			</div>
                			</div>
                        </div>
                    </div>
                    <div class="row-container light bg-scroll" style="">
					<div class="waves-container container">
						<div class="row">
							<div class="col-md-12 ">
								<div class="row">
									<div class="tw-element carousel-container col-md-12" style="">
										<div class="waves-carousel-post list_carousel carousel-anim" data-autoplay="false" data-items="4">
											<div class="waves-carousel">
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-sms.png" /><br />
                                                        <div id="ServicesTitle">SMS</div>
                                                        <p class="pServices">Llega a tus contactos de forma instantánea, donde quiera que estén e independientemente de si están usando un smartphone o un teléfono regular.</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtn">
                                                        		<a href="sms-masivo" class="TextBlank" title="Ver Más">Ver más</a>
                                                        	</div>
                                                     	</div>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-email.png" /><br />
                                                        <div id="ServicesTitle">
                                                        	E-mail
                                                        </div>
                                                        <p class="pServices">Envía mensajes sin límites de contenido y fáciles de compartir a través de uno de los medios de marketing mas costo-efectivo que existe.</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtn">
                                                        	<a href="mail-masivo" class="TextBlank" title="Ver Más">Ver más</a>
                                                        	</div>
                                                        </div>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-ivr.png" /><br />
                                                        <div id="ServicesTitle">
                                                        	IVR
                                                        </div>
                                                        <p class="pServices">Genera diálogos con textos, imágenes, audios y videos, y sin límites de caracteres a través del servicio de mensajería instantánea por excelencia.</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtnInactive">
                                                        		próximamente
                                                        	</div>
                                                        </div>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-whatsapp.png" /><br />
                                                        <div id="ServicesTitle">
                                                        	Whatsapp
                                                        </div>
                                                        <p class="pServices">Diminuye los costos operacionales de tu empresa e incrementa los niveles de eficiencia y satisfacción en la atención a clientes.</p><br />
                                                        <div id="MyBtnplace">
                                                        	<div id="MyBtnInactive">
                                                        		Próximamente
                                                        	</div>
                                                        </div>
													</div>
												</div>
											</div>
											<div class="clearfix">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                </div>
                <!--End New Services Responsive-->
                <!--Start New Plataform Responsive-->
                <div id="MainPlataform">
                	<div class="col-md-12">
						<div class="row">
                        	<div id="SectionTitle">
                            	Nuestra Plataforma
                            </div>
                        </div>
                    </div>
                    <div class="row-container light bg-scroll" style="">
					<div class="waves-container container">
						<div class="row">
							<div class="col-md-12 ">
								<div class="row">
									<div class="tw-element carousel-container col-md-12" style="">
										<div class="waves-carousel-post list_carousel carousel-anim" data-autoplay="false" data-items="6">
											<div class="waves-carousel">
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-responsiva.png" /><br />
                                                        <div id="PlataformTitle">Web & Responsive</div>
                                                        <p class="pPlataform">Accede a nuestra plataforma desde tu PC, celular o tablet, donde quiera que estés.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-autogestion.png" /><br />
                                                        <div id="PlataformTitle">Autogestionable</div>
                                                        <p class="pPlataform">Es simple e intuitiva. Gestiona tus campañas sin la necesidad de intermediarios.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-soporte.png" /><br />
                                                        <div id="PlataformTitle">Soporte</div>
                                                        <p class="pPlataform">Nuestros ejecutivos están disponibles para ayudarte cuando así lo necesites.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-reportes.png" /><br />
                                                        <div id="PlataformTitle">Reportes en Línea</div>
                                    <p class="pPlataform">Reportes en tiempo real y herramientas en linea para la toma de decisiones.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-integracion.png" /><br />
                                                        <div id="PlataformTitle">Integración vía API</div>
                                                        <p class="pPlataform">Integra tu sistema de gestión utilizando nuestra API en forma rápida y segura.</p>
													</div>
												</div>
												<div class="tw-owl-item">
													<div class="carousel-thumbnail waves-thumbnail">
														<img src="img/icon-ready.png" /><br />
                                                        <div id="PlataformTitle">Ready to work</div>
                                                        <p class="pPlataform">Prueba la demo gratis, compra créditos vía webpay y envía tus campañas.</p>
													</div>
												</div>
											</div>
											<div class="clearfix">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                </div>
                <!--End New Plataform Responsive-->
                <!--Start Callout-->
				<div id="MyCallOut">
					<div class="waves-container container">
						<div class="row">
                        	<div class="col-md-6">
                            	<div id="CallOutText">
                                	<p>Empieza hoy a usar Connectus</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                            	<div id="CallOutMobile">
                                <div id="CallOutBtn">
                                    <a href="/registro" target="_blank" class="CallOutBot">Prueba ahora nuestra plataforma</a>
                                </div>
                                </div>
                            </div>
                            <!--Start Older Callout
							<div class="col-md-12 ">
								<div class="row">

									<div class="tw-element waves-callout with-button style2 col-md-12">

										<div class="callout-container">

                                            <div class="callout-text clearfix">
												<p>Empieza hoy a usar Connectus</p>
												<a href="#" target="_blank" class="btn btn-callout btn-flat" style="border-color:#3B5998;background-color:#3B5998">Prueba ahora nuestra plataforma</a>
											</div>
                                            -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                <!--End Callout-->
                <!--End Content Connectus-->
			</div>
			<!-- End Main -->
