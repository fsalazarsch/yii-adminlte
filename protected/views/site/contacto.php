        <head>
        <?php
            $pag = 'contacto';
          ?>

        <style type="text/css">
            #txt_asunto{
                width: 100%;
            }
        </style>
    </head>
    <body class="page page-template-default custom-background waves-pagebuilder menu-fixed header-logo_left theme-full">

		<div id="theme-layout">


			<!-- Start Main -->
			<section id="main">
			<div id="page">
            	<!--Start Content Connectus-->
                <!--Start Page Header-->
                	<div id="PageHeader">
                    	<div class="container">
                        	<div class="waves-breadcrumbs">
								<div id="crumbs" class="">
								<span class="crumb-item"><a href="index.html">Homepage</a></span><span class="crumb-item current">Contacto</span>
                                </div>
                            </div>
                        	<div class="col-md-6"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/contact-header-image.png" /></div>
                            <div class="col-md-6">
                                <div id="PageData"><br /><div id="PageTitle">COMUNÍCATE CON NOSOTROS</div><br />
                                Nuestros ejecutivos se pondrán en contacto<br>
                                contigo durante el transcurso del día.
								</div>
							</div>
                        </div>
                    </div>
                <!--End Page Header-->
                <!--Start Form-->
                	<div id="PortWhy">
                        <!--Start Contact Content-->
                        	<div class="row-container light bg-scroll" style="">
								<div class="waves-container container">
									<div class="row">
										<div class="col-md-12">
											<!--Start Form Title-->
                                			<div class="row">
												<div id="SectionTitleBlue">
													Déjanos tu mensaje
												</div>
											</div>
                                			<!--End Form Title-->
										<div class="row">
<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success alert">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>
<?php endif; ?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		
		<?php echo $form->textField($model,'name', array('style' => 'margin: 10px 20px 10px 0; display: inline-block;', 'placeholder' => 'Nombre completo')); ?>
		<?php echo $form->textField($model,'email', array('style' => 'margin: 10px 20px 10px 0; display: inline-block;', 'placeholder' => 'Email')); ?>
		<?php echo $form->textField($model,'telefono', array('style' => 'margin: 10px 20px 10px 0; display: inline-block;', 'placeholder' => 'Telefono')); ?>
	</div>

	<div class="row">
		<?php echo $form->textField($model,'subject',array('style' => 'margin: 10px 20px 10px 0; display: inline-block; width:90%;','maxlength'=>128, 'placeholder' => 'Asunto')); ?>
	</div>

	<div class="row">
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50, 'style' => 'margin: 10px 20px 10px 0; display: inline-block; width:90%;', 'placeholder' => 'Escribe aqui tu mensaje')); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode', array('style' => 'margin: 10px 20px 10px 0; display: inline-block; width:90%;')); ?>
		</div>
		<div class="hint">Por favor ingrese las letras del captcha.
		<br/>No se distingen entre mayusculas y minusculas.</div>
	</div>
	<?php endif; ?>

	<div class="row submit">
		<?php echo CHtml::submitButton('ENVIAR', array('style' => 'border-color: #1f90d4;background-color: #1f90d4;')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->



												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</div>
                            <!--End Contact Content-->
                    </div>
                <!--End Form-->



                <!--End Content Connectus-->
			</div>
			</section>
			<!-- End Main -->
