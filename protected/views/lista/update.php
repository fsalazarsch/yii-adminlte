<?php
$this->breadcrumbs=array(
	'Listas'=>array('index'),
	$model->id_lista=>array('view','id'=>$model->id_lista),
	'Modificar',
);
?>

<h1>Modificar lista #<?php echo $model->id_lista; ?></h1>
<?
	$contactos = Yii::app()->db2->createCommand('Select * from contacto where id_lista = '.$model->id_lista)->queryAll();
	echo '<table>';
	foreach($contactos as $c){
		echo '<tr>';
		echo '<td>'.$c['nombre'].'</td>';
		echo '<td>'.$c['email'].'</td>';
		echo '<td>'.$c['celular'].'</td>';
		echo '<td>'.$c['estado'].'</td>';

		echo '</tr>';
		}
	echo '</table>';
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'conector-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id_conector',
		array(
		'name' => 'nombre',
		'value'=> 'contacto::model()->findByPk($model->id_lista)->nombre',
		),
		
		array(
    'class'=>'CButtonColumn',
    			'template' => '{editar} {borrar}',
    			'htmlOptions' => array('style' => 'width: 170px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(

				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("conector/update", array("id"=>$data->id_conector))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("conector/delete", array("id"=>$data->id_conector))',
					
				),
				),
    ),
	),
)); ?>

<?php// echo $this->renderPartial('_form', array('model'=>$model)); ?>
