<?
function get_empresa($id_empresa){
	$co = Yii::app()->db->createCommand('Select count(*) from assert_customer where customer_id = '.$id_empresa)->queryScalar();
	if($co == 0)
		return '---';
	else
		return cliente::model()->findByPk($id_empresa)->razon_social;
	}

function get_user($id_usuario){
	$co = Yii::app()->db->createCommand('Select count(*) from assert_user where user_id = '.$id_usuario)->queryScalar();
	if($co == 0)
		return '---';
	else
		return user::model()->findByPk($id_usuario)->username;
	}
	
function parsedate($fecha){
	return date('d-m-Y', strtotime($fecha));
	}
?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar lista</h2>
<div class="pull-right">
<a href="./create" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<a class="btn btn-success"><i class="fa fa-file-excel-o"></i></a>
</div>

</div>

<?
$this->breadcrumbs=array(
	'lista'=>array('index'),
	'Administrar',
);
?>
<!--a class="btn btn-default" data-toggle="tooltip" title="Mis Listas" data-original-title="Mis Listas"><i class="fa fa-mail-reply"></i></a-->
<?

Yii::app()->clientScript->registerScript('search', "


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#lista-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
",  CClientScript::POS_LOAD);
?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'lista-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
	'id_lista',
	'nombre',
	array(
	'name' => 'fecha_creacion',
    'value'=> 'parsedate($data->fecha_creacion)',
	),
	array(
	'name' => 'ultima_actualizacion',
    'value'=> 'parsedate($data->ultima_actualizacion)',
	),

	array(
	'name' => 'id_empresa',
    'value'=> 'get_empresa($data->id_empresa)',
	),
	array(
	'name' => 'id_usuario',
    'value'=> 'get_user($data->id_usuario)',
	),

	array(
	'name' => 'borrado',
    'value'=> '($data->borrado == 0)? "No" : "Si"',
	),
		
		array(
    'class'=>'CButtonColumn',
    			'template' => '{editar} {borrar}',
    			'htmlOptions' => array('style' => 'width: 220px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(
				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("lista/update", array("id"=>$data->id_lista))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("lista/update", array("id"=>$data->id_lista))',
					
				),
				),
    ),
	),
)); ?>
<br><br>

