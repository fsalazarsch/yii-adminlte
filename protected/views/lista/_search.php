<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--div class="form-group">
		< ?php echo $form->label($model,'customer_id'); ?>
		< ?php echo $form->numberField($model,'customer_id',array('size'=>60,'maxlength'=>128)); ?>
	</div-->
<table>
	<tr>
	<td>
	<div class="form-group">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>30,'maxlength'=>128)); ?>
	</div>
	</td>
	<td>
	<div class="form-group">
		<?php echo $form->label($model,'id_empresa'); ?>
		<?php echo $form->dropDownList($model,'id_empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'razon_social')), 'customer_id','razon_social'), array('empty'=>'Seleccionar..')); ?>
	</div>
	</td>
	<td>

	<div class="form-group">
		<?php echo $form->label($model,'id_usuario'); ?>
		<?php echo $form->dropDownList($model,'id_usuario', CHtml::listData(user::model()->findAll(array('order'=>'username')), 'user_id','username'), array('empty'=>'Seleccionar..')); ?>
	</div>
	</td>
	<td>

	<div class="form-group">
		<?php echo $form->label($model,'borrado'); ?>
		<?php echo $form->checkBox($model,'borrado'); ?>
	</div>

	</td>
	</tr>
	</table>
	<div class="form-group"> 
		<?php echo CHtml::submitButton('Buscar',  array('class' => 'btn btn-primary')); ?>
		
		
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
