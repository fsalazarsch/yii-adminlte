

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data', 'role'=> 'form'),
)); ?>

	
<div class="box-body">
	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'nombre', array('class'=>'help-block')); ?>
	</div>
	
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_empresa'); ?>
		<?php echo $form->dropDownList($model,'id_empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'razon_social')), 'customer_id','razon_social'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_empresa', array('class'=>'help-block')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_usuario'); ?>
		<?php echo $form->dropDownList($model,'id_usuario', CHtml::listData(user::model()->findAll(array('order'=>'username')), 'user_id','username'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_usuario', array('class'=>'help-block')); ?>
	</div>

		
	<div class="row box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
