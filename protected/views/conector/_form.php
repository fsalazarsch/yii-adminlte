
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'conector-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre', array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'glosa'); ?>
		<?php echo $form->textField($model,'glosa', array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'glosa'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'ip_servidor'); ?>
		<?php echo $form->textField($model,'ip_servidor', array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'ip_servidor'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'defecto'); ?>
		<?php echo $form->checkBox($model,'defecto'); ?>
		<?php echo $form->error($model,'defecto'); ?>
	</div>

	
	<div class="form-group">
		<?php echo $form->labelEx($model,'key'); ?>
		<?php echo $form->textField($model,'key', array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'key'); ?>
	</div>

	<div class="row box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
