<?php
$this->breadcrumbs=array(
	'Conectores'=>array('index'),
	'Crear',
);

?>

<div class="box box-primary box-header with-border">
<h3 class="box-title">Crear conector</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
