<?php
$this->breadcrumbs=array(
	'Conectores'=>array('index'),
	$model->id_conector=>array('view','id'=>$model->id_conector),
	'Modificar',
);

?>

<h1>Modificar conector '<?php echo $model->glosa; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
