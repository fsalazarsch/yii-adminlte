<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'id_conector'); ?>
		<?php echo $form->textField($model,'id_conector'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'glosa'); ?>
		<?php echo $form->textField($model,'glosa',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('Buscar', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
