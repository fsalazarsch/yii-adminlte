<?php
$this->breadcrumbs=array(
	'Conectores'=>array('index'),
	$model->id_conector,
);

$this->menu=array(
	array('label'=>'Listar conector', 'url'=>array('index')),
	array('label'=>'Crear conector', 'url'=>array('create')),
	array('label'=>'Modificar conector', 'url'=>array('update', 'id'=>$model->id_conector)),
	array('label'=>'Borrar conector', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_conector),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar conector', 'url'=>array('admin')),
);
?>

<h1>Mostrando conector '<?php echo $model->glosa; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tipo',
		'nombre',
		'glosa',
	),
)); ?>
