<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_conector')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_conector), array('view', 'id'=>$data->id_conector)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo')); ?>:</b>
	<?php echo CHtml::encode($data->tipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('glosa')); ?>:</b>
	<?php echo CHtml::encode($data->glosa); ?>
	<br />
	<br />

</div>
