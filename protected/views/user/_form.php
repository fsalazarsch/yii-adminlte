
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<div class="box box-primary box-header with-border">
<? if(!$model->isNewRecord){ ?>	
<h2 class="box-title">Administrar cliente</h2>
<?}
else{
?>
<h2 class="box-title">Registrar cliente</h2>
<?}?>

<div class="pull-right">
	<div class="row box-footer">
		<?echo CHtml::tag('button',
                array('class' => 'btn btn-large pull-right btn btn-primary', 'onclick'=> '"this.form.submit()"'),
                '<i class="fa fa-save"></i>');
                ?>
	</div>
</div>

</div>

<div class="box-body">

	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_empresa'); ?>
		<?php echo $form->dropDownList($model,'id_empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'razon_social')), 'customer_id','razon_social'), array('empty'=>'Seleccionar..', 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'empresa'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	
	<?
	if( Yii::app()->user->getIsAdmin()){
	?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'accessLevel'); ?>
		<?php echo $form->dropDownList($model,'accessLevel',$model->getAccessLevelList(), array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'accessLevel'); ?>
	</div>
	<?}?>
	
	<!--div class="form-group">
		< ?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar'); ?>
	</div-->

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
