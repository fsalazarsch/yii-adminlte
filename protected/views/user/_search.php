<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--div class="row">
		< ?php echo $form->label($model,'id'); ?>
		< ?php echo $form->textField($model,'id'); ?>
	</div-->
	<table>
		<tr>
		<td>
	<div class="form-group">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
	</div>
	</td>
	<td style="padding-left: 20px;">
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_empresa'); ?>
		<?php echo $form->dropDownList($model,'id_empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'razon_social')), 'customer_id','razon_social'), array('empty'=>'Seleccionar..', 'class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->label($model,'date_added'); ?>
		<?php echo $form->dateField($model,'date_added',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::label('Activo','activo'); ?>
		<?php echo $form->checkBox($model,'status',array('size'=>60,'maxlength'=>128)); ?>
	</div>
		</td>
		</tr>
	</table>
	<div class="form-group">
		<?php echo CHtml::submitButton('Buscar', array('class' => 'btn btn-primary')); ?>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- search-form -->
