<?php
function get_empresa($id_empresa){
	$co = Yii::app()->db->createCommand('Select count(*) from assert_customer where customer_id = '.$id_empresa)->queryScalar();
	if($co == 0)
		return '---';
	else
		return cliente::model()->findByPk($id_empresa)->razon_social;
	}

function parsedate($fecha){
	return date('d-m-Y', strtotime($fecha));
	}

	
$this->breadcrumbs=array(
	'Usuarios',
);

?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar Usuarios</h2>
<div class="pull-right">
<a href="<? echo Yii::app()->createUrl("user/create"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<a href="<? echo Yii::app()->createUrl("user/excel"); ?>" class="btn btn-success"><i class="fa fa-file-excel-o"></i></a>
</div>
</div>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		//'id',
		'username',
		'email',
		array(
		'name' => 'id_empresa',
		'value' => 'get_empresa($data->id_empresa)',
		),

		array(
		'name' => 'date_added',
		'value' => 'parsedate($data->date_added)',
		),

		array(
		'name' => 'status',
		'value' => '($data->status == 1) ? "Activo" : "Inactivo"',
		),
		
		
		array(
		'name' => 'accessLevel',
		'value' => '($data->accessLevel == 100) ? "Root" : (($data->accessLevel == 99) ? "Administrador" : (($data->accessLevel == 55) ? "Coordinador General" : (($data->accessLevel == 50) ? "Ejecutivo" : "Contacto" )))',
		),
				
		array(
			'class'=>'CButtonColumn',
			'template' => '{editar} {borrar}',
    		'htmlOptions' => array('style' => 'width: 170px;'),
    		 'buttons'=>array
				(
				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("user/update", array("id"=>$data->user_id))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("user/delete", array("id"=>$data->user_id))',
					
				),
				),
    ),
	),
)); ?>
<br><br>

