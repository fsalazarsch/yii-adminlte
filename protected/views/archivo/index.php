<?php
$this->breadcrumbs=array(
	'archivoes',
	'Administrar',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#archivo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar archivoes</h2>
<div class="pull-right">
</div>

</div>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'archivo-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id_archivo',
		array(
    'class'=>'CButtonColumn',
    			'template' => '{descargar} {borrar}',
    			'htmlOptions' => array('style' => 'width: 170px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(

				'descargar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("archivo/update", array("id"=>$data->id_archivo))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("archivo/delete", array("id"=>$data->id_archivo))',
					
				),
				),
    ),
	),
)); ?>
