<?php
function get_empresa($id_empresa){
	$co = Yii::app()->db->createCommand('Select count(*) from assert_customer where customer_id = '.$id_empresa)->queryScalar();
	if($co == 0)
		return '---';
	else
		return cliente::model()->findByPk($id_empresa)->razon_social;
	}
	?>
<?php
$this->breadcrumbs=array(
	'Contactos',
	'Administrar',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#contacto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar contactoes</h2>
<div class="pull-right">
<a href="<? echo Yii::app()->createUrl("contacto/create"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<a href="<? echo Yii::app()->createUrl("contacto/excel"); ?>" class="btn btn-success"><i class="fa fa-file-excel-o"></i></a>
</div>

</div>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contacto-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id_contacto',
		'nombre',
		'email',
		'celular',
		array(
		'name' => 'id_lista',
		'value'=> 'lista::model()->findByPk($data->id_lista)->nombre',
		),

		array(
		'name' => 'empresa',
		'value'=> 'get_empresa(lista::model()->findByPk($data->id_lista)->id_empresa)',
		),

		'id_lista',
		array(
    'class'=>'CButtonColumn',
    			'template' => '{editar} {borrar}',
    			'htmlOptions' => array('style' => 'width: 170px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(

				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("contacto/update", array("id"=>$data->id_contacto))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("contacto/delete", array("id"=>$data->id_contacto))',
					
				),
				),
    ),
	),
)); ?>
