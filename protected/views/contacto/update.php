<?php
$this->breadcrumbs=array(
	'Contacto'=>array('index'),
	$model->id_contacto=>array('view','id'=>$model->id_contacto),
	'Modificar',
);

?>

<h1>Modificar contacto '<?php echo $model->id_contacto; ?>'</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
