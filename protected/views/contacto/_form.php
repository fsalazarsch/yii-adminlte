
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'conector-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="box-body">

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email', array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->numberField($model,'celular', array('size'=>50,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'celular'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->checkBox($model,'estado'); ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_lista'); ?>
		<?php //echo $form->dropDownList($model,'id_lista', CHtml::listData(lista::model()->findAll(array('condition' => 'tipo = "SMS"', 'order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->dropDownList($model,'id_lista', CHtml::listData(lista::model()->findAll(array('order'=>'nombre')), 'id_lista','nombre'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_lista', array('class'=>'help-block')); ?>
	</div>


	<div class="row box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
