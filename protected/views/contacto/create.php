<?php
$this->breadcrumbs=array(
	'Contacto'=>array('index'),
	'Crear',
);

?>

<div class="box box-primary box-header with-border">
<h3 class="box-title">Crear contacto</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
