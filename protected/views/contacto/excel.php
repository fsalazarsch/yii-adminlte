<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("connectus")
    ->setLastModifiedBy("connectus");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Conectores');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID CONECTOR');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'TIPO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'NOMBRE');			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'GLOSA');
				
				
		
				$conectores = Yii::app()->db->createCommand('SELECT * FROM assert_conector')->queryAll();
				$i=0;
				foreach($conectores as $c){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $c['id_conector']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $c['tipo']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $c['nombre']);			
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $c['glosa']);
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Conectores.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
