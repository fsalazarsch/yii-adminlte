<?php
$this->breadcrumbs=array(
	'contactoes'=>array('index'),
	$model->id_contacto,
);

$this->menu=array(
	array('label'=>'Listar contacto', 'url'=>array('index')),
	array('label'=>'Crear contacto', 'url'=>array('create')),
	array('label'=>'Modificar contacto', 'url'=>array('update', 'id'=>$model->id_contacto)),
	array('label'=>'Borrar contacto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_contacto),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar contacto', 'url'=>array('admin')),
);
?>

<h1>Mostrando contacto '<?php echo $model->id_contacto; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tipo',
		'nombre',
		'glosa',
	),
)); ?>
