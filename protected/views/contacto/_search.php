<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>50,'maxlength'=>128)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email', array('size'=>50,'maxlength'=>128)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->numberField($model,'celular', array('size'=>50,'maxlength'=>128)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->checkBox($model,'estado'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_lista'); ?>
		<?php //echo $form->dropDownList($model,'id_lista', CHtml::listData(lista::model()->findAll(array('condition' => 'tipo = "SMS"', 'order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->dropDownList($model,'id_lista', CHtml::listData(lista::model()->findAll(array('order'=>'nombre')), 'id_lista','nombre'), array('empty'=>'Seleccionar..')); ?>
	</div>
	
	<div class="form-group">
		<?php echo CHtml::submitButton('Buscar', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
