<?php
$this->breadcrumbs=array(
	'Conectores',
	'Administrar',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#conector-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar Grupos</h2>
<div class="pull-right">
<a href="<? echo Yii::app()->createUrl("grupo/create"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<a href="<? echo Yii::app()->createUrl("grupo/excel"); ?>" class="btn btn-success"><i class="fa fa-file-excel-o"></i></a>
</div>
</div>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'grupo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'customer_group_id',
		'name',
		'description',
		array(
    'class'=>'CButtonColumn',
    			'template' => '{editar} {borrar}',
    			'htmlOptions' => array('style' => 'width: 170px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(

				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("grupo/update", array("id"=>$data->customer_group_id))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("grupo/delete", array("id"=>$data->customer_group_id))',
					
				),
				),
    ),
	),
)); ?>
