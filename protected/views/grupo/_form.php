
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'grupo-form',
	'enableAjaxValidation'=>false,
)); ?>

<div class="box box-primary box-header with-border">
<? if(!$model->isNewRecord){ ?>	
<h2 class="box-title">Editar grupo</h2>
<?}
else{
?>
<h2 class="box-title">Registrar grupo</h2>
<?}?>

<div class="pull-right">
	<div class="row box-footer">
		<?echo CHtml::tag('button',
                array('class' => 'btn btn-large pull-right btn btn-primary', 'onclick'=> '"this.form.submit()"'),
                '<i class="fa fa-save"></i>');
                ?>
	</div>
</div>

</div>


<div class="box-body">

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>



	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description', array('size'=>50,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<!--div class="row box-footer">
		< ?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',  array('class' => 'btn btn-primary')); ?>
	</div-->

<?php $this->endWidget(); ?>

</div><!-- form -->
