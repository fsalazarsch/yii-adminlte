<?php
$this->breadcrumbs=array(
	'Grupos'=>array('index'),
	$model->customer_group_id,
);

?>

<h1>Mostrando grupo '<?php echo $model->name; ?>'</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'description',
		
	),
)); ?>
