<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_group_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->customer_group_id), array('view', 'id'=>$data->customer_group_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />
	<br />

</div>
