<?php
function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("connectus")
    ->setLastModifiedBy("connectus");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Grupos');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID GRUPO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'NOMBRE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'DESCRIPCION');
				
				
		
				$conectores = Yii::app()->db->createCommand('SELECT * FROM assert_customer_group_description')->queryAll();
				$i=0;
				foreach($conectores as $c){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $c['customer_group_id']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), $c['name']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $c['description']);			
				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Grupos.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
