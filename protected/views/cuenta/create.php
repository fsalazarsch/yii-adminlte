<?php
$this->breadcrumbs=array(
	'Cuenta'=>array('index'),
	'Crear',
);

?>

<div class="box box-primary box-header with-border">
<h3 class="box-title">Registrar Cuenta</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
