<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<table>
	<tr>
	<td>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_empresa'); ?>
		<?php echo $form->dropDownList($model,'id_empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'razon_social')), 'customer_id','razon_social'), array('empty'=>'Seleccionar..')); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_conector_sms'); ?>
		<?php echo $form->dropDownList($model,'id_conector_sms', CHtml::listData(conector::model()->findAll(array('condition' => 'tipo = "SMS"', 'order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..')); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_conector_mail'); ?>
		<?php echo $form->dropDownList($model,'id_conector_mail', CHtml::listData(conector::model()->findAll(array('condition' => 'tipo = "MAIL"','order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..')); ?>
	</div>
	
	</td>
	</tr>
	</table>



	<div class="form-group">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
