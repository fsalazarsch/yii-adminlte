
<?php
function get_empresa($id_empresa){
	$co = Yii::app()->db->createCommand('Select count(*) from assert_customer where customer_id = '.$id_empresa)->queryScalar();
	if($co == 0)
		return '---';
	else
		return cliente::model()->findByPk($id_empresa)->razon_social;
	}
// we use header of button column for the drop down
// so change the CButtonColumn in columns array like this:
?>


<div class="box box-primary box-header with-border">
<h2 class="box-title">Administrar Cuentas</h2>
<div class="pull-right">
<a href="<? echo Yii::app()->createUrl("cuenta/create"); ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<a href="<? echo Yii::app()->createUrl("cuenta/excel"); ?>" class="btn btn-success"><i class="fa fa-file-excel-o"></i></a>
</div>
</div>


<?
$this->breadcrumbs=array(
	'Cuenta',
	'Administrar',
);
?>
<!--a class="btn btn-default" data-toggle="tooltip" title="Mis Listas" data-original-title="Mis Listas"><i class="fa fa-mail-reply"></i></a-->
<?
Yii::app()->clientScript->registerScript('search', "

$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cuenta-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cuenta-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
	'id_cuenta_corriente', 
	'saldo_credito', 
	array(
		'name'=>'id_empresa',
        'value'=> 'get_empresa($data->id_empresa)',								 
        ), 
	'saldo_mail', 
	'saldo_sms', 
	'consumidos_mail', 
	'consumidos_sms',
	array(
		'name'=>'id_conector_sms',
        'value'=> 'conector::model()->findByPk($data->id_conector_sms)->glosa',								 
        ), 
	array(
		'name'=>'id_conector_mail',
        'value'=> 'conector::model()->findByPk($data->id_conector_mail)->glosa',								 
        ), 
		
		array(
    'class'=>'CButtonColumn',
    			'template' => '{editar} {borrar}',
    			'htmlOptions' => array('style' => 'width: 170px;'),
    			//'filterHtmlOptions' => array('style' => 'width: 30px;'),
			  'buttons'=>array
				(
				'editar' => array
				(
					'label'=>'<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>',
					 'options'=>array('title'=>''),
					 'url'=>'Yii::app()->createUrl("cuenta/update", array("id"=>$data->id_cuenta_corriente))',
						
				),	
				'borrar' => array
				(
					'label'=>'<button class="btn btn-danger"><i class="fa fa-trash"></i></button>',
					'options'=>array('title'=>''),
					'url'=>'Yii::app()->createUrl("cuenta/delete", array("id"=>$data->id_cuenta_corriente))',
					
				),
				),
    ),
	),
)); ?>
<br><br>

