<?php
Yii::app()->clientScript->registerScript('search', "

$('#saldo_mail2').change(function(){
	alert('asd');
	
	$('#saldo_mail2').val()+$('#cuenta_saldo_mail').val()
});

",  CClientScript::POS_READY);

$this->breadcrumbs=array(
	'Cliente'=>array('cliente/index'),
	'Modificar Saldo',
);

?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Modificar Saldo del cliente <?php echo cliente::model()->findbyPk($model->id_empresa)->razon_social;?></h2>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data', 'role'=> 'form'),
)); ?>

<div class="pull-right">
	<div class="row box-footer">
		<?echo CHtml::tag('button',
                array('class' => 'btn btn-large pull-right btn btn-primary', 'onclick'=> '"this.form.submit()"'),
                '<i class="fa fa-save"></i>');
                ?>
		</div>
	</div>
</div>
<h4><? echo cliente::model()->findbyPk($model->id_empresa)->email ?></h4>



<div class="box-body">
	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	
		<table width="90%">
		<tr>
			<td width="75%" style="padding-left:5%">
	<div class="form-group">
		<?php echo $form->labelEx($model,'Cred Mail Restantes'); ?>
		<?php echo $form->numberField($model,'saldo_mail',array('class'=>'form-control', 'style'=>'width:100%;' ,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saldo_mail', array('class'=>'help-block')); ?>
		
		<?php echo $form->labelEx($model,'Mail a cargar'); ?>
		<?php echo CHtml::numberField('saldo_mail2','',array('class'=>'form-control', 'style'=>'width:100%;' , 'onkeyup'=> '$("#cantmail").text( Number($("#saldo_mail2").val())+Number($("#cuenta_saldo_mail").val()) )')); ?>
	</div>
		</td>
		<td width="25%" style="padding-left:5%">
		<div style="display: inline-block;">
			<div style="font-size: xx-large;" id="cantmail">
				<? echo $model->saldo_mail; ?> 
			</div>
			<i class="fa fa-envelope"></i>
		</div>
		</td>
	</tr>
	</table>
		<table width="90%">
		<tr>
			<td width="75%" style="padding-left:5%">
	<div class="form-group">
		<?php echo CHtml::label('Cred SMS Restantes','Cred SMS Restantes'); ?>
		<?php echo $form->numberField($model,'saldo_sms',array('class'=>'form-control', 'style'=>'width:100%;' ,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saldo_sms', array('class'=>'help-block')); ?>
		
		<?php echo CHtml::label('SMS a cargar','SMS a cargar'); ?>
		<?php echo CHtml::numberField('saldo_sms2','',array('class'=>'form-control', 'style'=>'width:100%;' , 'onkeyup'=> '$("#cantsms").text( Number($("#saldo_sms2").val())+Number($("#cuenta_saldo_sms").val()) )')); ?>
	</div>
		</td>
		<td width="25%" style="padding-left:5%">
		<div style="display: inline-block;">
			<div style="font-size: xx-large;" id="cantsms">
				<? echo $model->saldo_sms; ?> 
			</div>
			<i class="fa fa-comments"></i>
		</div>
		</td>
	</tr>
	</table>
	
	
	<!--div class="row box-footer">
		< ?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar', array('class' => 'btn btn-primary')); ?>
	</div-->

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
