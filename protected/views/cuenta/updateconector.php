<?php
$this->breadcrumbs=array(
	'Cliente'=>array('cliente/index'),
	'Modificar conectores',
);

?>

<div class="box box-primary box-header with-border">
<h2 class="box-title">Modificar Cuenta del cliente <?php echo cliente::model()->findbyPk($model->id_empresa)->razon_social;?></h2>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data', 'role'=> 'form'),
)); ?>

<div class="pull-right">
	<div class="row box-footer">
		<?echo CHtml::tag('button',
                array('class' => 'btn btn-large pull-right btn btn-primary', 'onclick'=> '"this.form.submit()"'),
                '<i class="fa fa-save"></i>');
                ?>
		</div>
	</div>
</div>
<h4><? echo cliente::model()->findbyPk($model->id_empresa)->email ?></h4>


<div class="box-body">
	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_conector_sms'); ?>
		<?php echo $form->dropDownList($model,'id_conector_sms', CHtml::listData(conector::model()->findAll(array('condition' => 'tipo = "SMS"', 'order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..', 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'id_conector_sms', array('class'=>'help-block')); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_conector_mail'); ?>
		<?php echo $form->dropDownList($model,'id_conector_mail', CHtml::listData(conector::model()->findAll(array('condition' => 'tipo = "MAIL"','order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..', 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'id_conector_mail', array('class'=>'help-block')); ?>
	</div>
	
	
	<!--div class="row box-footer">
		< ?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar', array('class' => 'btn btn-primary')); ?>
	</div-->

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
