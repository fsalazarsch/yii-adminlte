<?php
function get_empresa($id_empresa){
	$co = Yii::app()->db->createCommand('Select count(*) from assert_customer where customer_id = '.$id_empresa)->queryScalar();
	if($co == 0)
		return '---';
	else
		return cliente::model()->findByPk($id_empresa)->razon_social;
	}

function autosize($objPHPExcel){
	for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
       ->getColumnDimension($col)
        ->setAutoSize(true);
	}
}

$this->layout=false;	
	Yii::import('application.extensions.phpexcel.PHPExcel');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("connectus")
    ->setLastModifiedBy("connectus");	
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Cuentas corrientes');
	autosize($objPHPExcel);
	//$j++;
	
			$formato_header = array(
				'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '00B0F0')
				),
				'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF')
				),

				);

			$formato_bordes = array(
					'borders' => array(
			'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			),
			));

			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($formato_header);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($formato_bordes);

			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID CUENTA CORRIENTE');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'EMPRESA');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'SALDO CREDITO');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'SALDO MAIL');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'SALDO SMS');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'MAIL CONSUMIDOS');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'SMS CONSUMIDOS');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'CONECTOR SMS');
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'CONECTOR MAIL');
				
				
			$ctas = Yii::app()->db->createCommand('SELECT * FROM assert_cuenta_corriente')->queryAll();
				
				$i=0;
				foreach($ctas as $c){
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($i+2), $c['id_cuenta_corriente']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($i+2), get_empresa($c['id_empresa']));
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($i+2), $c['saldo_credito']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($i+2), $c['saldo_mail']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($i+2), $c['saldo_sms']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($i+2), $c['consumidos_mail']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($i+2), $c['consumidos_sms']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($i+2), conector::model()->findByPk($c['id_conector_sms'])->glosa);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, ($i+2), conector::model()->findByPk($c['id_conector_mail'])->glosa);

				$i++;
				}
	

	


    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Cuentas.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>
