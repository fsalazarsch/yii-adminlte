

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data', 'role'=> 'form'),
)); ?>

<div class="box-body">
	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_empresa'); ?>
		<?php echo $form->dropDownList($model,'id_empresa', CHtml::listData(cliente::model()->findAll(array('order'=>'razon_social')), 'customer_id','razon_social'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_empresa', array('class'=>'help-block')); ?>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'saldo_mail'); ?>
		<?php echo $form->numberField($model,'saldo_mail',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saldo_mail', array('class'=>'help-block')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'saldo_sms'); ?>
		<?php echo $form->numberField($model,'saldo_sms',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'saldo_sms', array('class'=>'help-block')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'consumidos_mail'); ?>
		<?php echo $form->numberField($model,'consumidos_mail',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'consumidos_mail', array('class'=>'help-block')); ?>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'consumidos_sms'); ?>
		<?php echo $form->numberField($model,'consumidos_sms',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'consumidos_sms', array('class'=>'help-block')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'id_conector_sms'); ?>
		<?php echo $form->dropDownList($model,'id_conector_sms', CHtml::listData(conector::model()->findAll(array('condition' => 'tipo = "SMS"', 'order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_conector_sms', array('class'=>'help-block')); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'id_conector_mail'); ?>
		<?php echo $form->dropDownList($model,'id_conector_mail', CHtml::listData(conector::model()->findAll(array('condition' => 'tipo = "MAIL"','order'=>'glosa')), 'id_conector','glosa'), array('empty'=>'Seleccionar..')); ?>
		<?php echo $form->error($model,'id_conector_mail', array('class'=>'help-block')); ?>
	</div>
	
	
	<div class="row box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
