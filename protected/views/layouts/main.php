<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="es" />

	<?php
    
    # acá se configurará las etiquetas meta de cada página
    # se resivira una variable $pag, donde indicamos la página de la cual se proviene !
	$pag = 'index';
	$ver = '1.0.1';
    $keywords   = 'SMS, Email, Mail, Envíos, Masivos, Campañas, Chile, Marketing';

    switch ($pag) {

        case 'index':
            $titulo     = 'Expertos en envíos de mail y sms masivos';
            $desc       = 'Envíos masivos de SMS y Mail, Prueba ahora nuestra plataforma web de forma gratuita y confía en nosotros tus futuras campañas.';
        break;

        case 'sms':
            $titulo     = 'SMS Masivo';
            $desc       = 'Envíos masivos de mensaje de textos, Llega a tus contactos de forma instantánea y efectiva donde quiera que estén.';
        break;

        case 'email':
            $titulo     = 'Mail Masivo';
            $desc       = 'Envíos masivos de Mail, sin limites de contenido y fáciles de compartir mediante uno de los medios de marketing más costo-efectivo que existe.';
        break;

        case 'plataforma':
            $titulo     = 'Administra tus envíos de Mail y SMS masivos de forma eficiente';
            $desc       = 'Administra tus envíos en un solo lugar, desarrollado con los más altos estándares de tecnología, facilitando la gestión de bases de datos, mensajes y reportes.';
        break;

        case 'contacto':
            $titulo     = 'Comunícate con nosotros';
            $desc       = 'Comunícate con nosotros, nuestros ejecutivos se pondrán en contacto contigo durante el transcurso del día.';
        break;
        

    }
    

?>
<meta charset='utf-8'>
<title><?php echo $titulo; ?> | Connectus</title>

<meta name="robots" content="index, follow" />
<meta name="Description"    content="<?php echo $desc;      ?>" />
<meta name="Keywords"       content="<?php echo $keywords;  ?>" />


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="alternate" hreflang="es" href="http://es.example.com/" />

<!-- META Verificación Sitio Google -->
<meta name="google-site-verification" content="vaLnG1S1lVkJ9z3DINwk93vP-zWbbApTfQDABdV8GUY" />





<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/mmenu.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/prettyPhoto.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css' type='text/css' media='all' />

<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/style.css?ver=<?php echo $ver; ?>' type='text/css' media='all' />

<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/responsive.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/tw_woocommerce.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/twentytwenty.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/blue_skin.css' type='text/css' media='all' />

<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/connectus.css?ver=<?php echo $ver; ?>' type='text/css' media='all' /> 

<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/favicon.png" type="image/x-icon" />    
<!--[if IE]><link rel="SHORTCUT ICON" href="images/favicon.ico"/><![endif]--><!-- Internet Explorer-->
<link rel='apple-touch-icon' type='images/png' href='<?php echo Yii::app()->request->baseUrl; ?>/img/icon.57.png'> <!-- iPhone -->
<link rel='apple-touch-icon' type='images/png' sizes='72x72' href='<?php echo Yii::app()->request->baseUrl; ?>/img/icon.72.png'> <!-- iPad -->
<link rel='apple-touch-icon' type='images/png' sizes='114x114' href='<?php echo Yii::app()->request->baseUrl; ?>/img/icon.114.png'> <!-- iPhone4 -->

<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/favicon.png" type="image/x-icon" />    
<!--[if IE]><link rel="SHORTCUT ICON" href="images/favicon.ico"/><![endif]--><!-- Internet Explorer-->
<link rel='apple-touch-icon' type='images/png' href='<?php echo Yii::app()->request->baseUrl; ?>/img/icon.57.png'> <!-- iPhone -->
<link rel='apple-touch-icon' type='images/png' sizes='72x72' href='<?php echo Yii::app()->request->baseUrl; ?>/img/icon.72.png'> <!-- iPad -->
<link rel='apple-touch-icon' type='images/png' sizes='114x114' href='<?php echo Yii::app()->request->baseUrl; ?>/img/icon.114.png'> <!-- iPhone4 -->

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>


	<!-- Start Header -->
<div class="header-container">

    <header id="header" class="header-large">


        <div class="container">
            <div class="header row">
                <div class="div_top_left col-xs-6 ">
                    <span><i class="fa fa-phone-square"></i> +569 8449 2882</span>
                </div>

                <div class=" col-xs-7 col-xs-6 push_right">
                    <div class="tw-top-widget">
                        <span class="top-widget-title widgettitle">
                            <div id="BotAccount">
                                <a href="/registro" hreflang="es" class="AccountLink">crea tu cuenta gratuita</a>
                            </div>
                        </span>
                    </div>

                    <div class="tw-top-widget" id="wysija-2">
                        <span class="top-widget-title widgettitle">
                            <div id="UserLog">
                                <a href="http://connecta.connectus.cl" hreflang="es"  target="_blank">Ingreso Usuarios</a>
                            </div>
                        </span>
                    </div>
                </div>  


            </div>

            <div class="show-mobile-menu clearfix">
                <a href="#" class="mobile-menu-icon">
                <span></span><span></span><span></span><span></span>
                </a>
            </div>
            <div class="row header">
                <div class="col-md-3">
                    <div class="tw-logo">
                        <a class="logo" href="/" hreflang="es" ><img class="logo-img" src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.png" title="Connectus"/></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <nav class="menu-container clearfix">
                        <div class="tw-menu-container">
                            <!--<a id="tw-nav-toggle" href="#"><span></span></a>-->
                            <ul id="menu" class="sf-menu">
                                <li class="menu-item current-menu-ancestor"><a href="/" hreflang="es" >Homepage</a></li>
                                <li  class="menu-item  menu-item-has-children"><a href="#" hreflang="es" >Servicios</a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item"><a href="sms_masivo" hreflang="es" >SMS</a></li>
                                        <li  class="menu-item"><a href="mail_masivo" hreflang="es" >E-mail</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a  href="plataforma" hreflang="es" >Plataforma</a></li>
                                <li class="menu-item"><a href="contacto" hreflang="es" >Contacto</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

        <nav id="mobile-menu">
            <ul id="menu-menu-1" class="clearfix">
                <li class="menu-item  current-menu-ancestor"><a href="/" hreflang="es" >Homepage</a></li>
                
                <li class="menu-item  menu-item-has-children"><a href="#" hreflang="es" >Servicios</a>
                    <ul class="sub-menu">
                        <li class="menu-item "><a href="sms-masivo" hreflang="es" >SMS</a></li>
                        <li class="menu-item "><a href="mail-masivo" hreflang="es" >E-mail</a></li>
                    </ul>
                </li>
                
                <li class="menu-item"><a href="plataforma" hreflang="es" >Plataforma</a></li>
                <li class="menu-item"><a href="contacto" hreflang="es" >Contacto</a></li>
            </ul>
        </nav>

        <div class="header-dropshadow"></div>
        
    </header>
    
    <div class="header-clone"></div>
</div>
<!-- End Header -->



	<?php echo $content; ?>

<div id="bottom">
    <!-- Start Container-->
    <div class="container">

        <div class="row">

            <div class='col-md-4'></div>

            <div class="col-md-4 center">
                <aside class="widget flickr_widget" id="contactinfo-2">
                <div id="FooterLogo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo-blank.png" /></div>
                <div id="FooterMn">
                    <a href="sms.html">sms</a> | <a href="email.html">e-mail</a> | ivr | whatsapp
                </div>
                <div id="FooterData">Santa Beatriz Nº 100, Oficina 501 - Providencia<br /><i class="fa fa-phone-square"></i> +569 8449 2882</div>
                </aside>
            </div>

            <div class="col-md-4 center">
                <aside class="widget flickr_widget" id="contactinfo-2">
                <div id="FooterLogo2"><img src="<?php echo Yii::app()->request->baseUrl; ?>/image/subtel-logo.jpg" /></div>
                </aside>
            </div>

        </div>
    </div>
    <!-- End Container -->

   
</div>

<a id="scrollUp" title="Scroll to top"><i class="fa fa-chevron-up" style="padding: 10px;"></i></a>

    <!--script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js'></script-->
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/revslider/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>

    <script type="text/javascript">
        /******************************************
            -   PREPARE PLACEHOLDER FOR SLIDER  -
        ******************************************/
        var setREVStartSize = function() {
            var tpopt = new Object();
                tpopt.startwidth = 1170;
                tpopt.startheight = 650;
                tpopt.container = jQuery('#rev_slider_10_1');
                tpopt.fullScreen = "off";
                tpopt.forceFullWidth="off";
            tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
        };
        /* CALL PLACEHOLDER */
        setREVStartSize();
        var tpj=jQuery;
        tpj.noConflict();
        var revapi10;
        tpj(document).ready(function() {
        if(tpj('#rev_slider_10_1').revolution == undefined){
            revslider_showDoubleJqueryError('#rev_slider_10_1');
        }else{
           revapi10 = tpj('#rev_slider_10_1').show().revolution(
            {   
                                        dottedOverlay:"none",
                delay:9000,
                startwidth:1170,
                startheight:650,
                hideThumbs:0,
                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:3,
                simplifyAll:"off",
                navigationType:"bullet",
                navigationArrows:"none",
                navigationStyle:"round",
                touchenabled:"on",
                onHoverStop:"on",
                nextSlideOnWindowFocus:"off",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                drag_block_vertical: false,
                keyboardNavigation:"on",
                navigationHAlign:"right",
                navigationVAlign:"center",
                navigationHOffset:50,
                navigationVOffset:20,
                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,
                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,
                shadow:0,
                fullWidth:"on",
                fullScreen:"off",
                                        spinner:"spinner2",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                forceFullWidth:"off",
                hideThumbsOnMobile:"off",
                hideNavDelayOnMobile:1500,
                hideBulletsOnMobile:"on",
                hideArrowsOnMobile:"off",
                hideThumbsUnderResolution:0,
                                        hideSliderAtLimit:0,
                hideCaptionAtLimit:768,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0                    });
                            }
        }); /*ready*/
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var waves_script_data = {"menu_padding":"33","menu_wid_margin":"28","blog_art_min_width":"230","pageloader":"0","header_height":"80"};
        /* ]]> */
    </script>
    

    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/themewaves.js'></script>
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/waves-script.js'></script>
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/contact-form.js'></script>
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/smoothscroll.js'></script>
    <script type='text/javascript' src='<?php echo Yii::app()->request->baseUrl; ?>/js/scripts.js'></script>

</body>
</html>
