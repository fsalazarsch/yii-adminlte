<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
);
?>

<div class="box box-primary box-header with-border">
<h3 class="box-title">Registrar Usuario</h3>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
