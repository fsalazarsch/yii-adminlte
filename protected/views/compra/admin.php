<?php
// we use header of button column for the drop down
// so change the CButtonColumn in columns array like this:
?>
<div class="page-header">

<h2>Administrar compras

<div class="pull-right">
<a href="./create" class="btn btn-primary"><i class="fa fa-plus"></i></a>
<a class="btn btn-danger"><i class="fa fa-trash"></i></a>
<a class="btn btn-success"><!--style="background-color: #8fbb6c;"--><i class="fa fa-file-excel-o"></i></a>
</div>
</h2>
</div>

<?
$this->breadcrumbs=array(
	'compras'=>array('index'),
	'Administrar',
);
?>
<!--a class="btn btn-default" data-toggle="tooltip" title="Mis Listas" data-original-title="Mis Listas"><i class="fa fa-mail-reply"></i></a-->
<br>
<?
Yii::app()->clientScript->registerScript('search', "

$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'compra-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_compra',
		'cantidad_creditos',
		
		array(
    'class'=>'CButtonColumn',
    ),
	),
)); ?>
<br><br>
