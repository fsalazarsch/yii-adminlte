

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'upload-form',
	'enableAjaxValidation'=>false,
	 'htmlOptions' => array('enctype' => 'multipart/form-data', 'role'=> 'form'),
)); ?>

<div class="box-body">
	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group has-error">
		<?php echo $form->labelEx($model,'cantidad_creditos'); ?>
		<?php echo $form->textField($model,'cantidad_creditos',array('class'=>'form-control', 'size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'cantidad_creditos', array('class'=>'help-block')); ?>
	</div>
	
	
	
	<div class="row box-footer">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Agregar' : 'Guardar', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<br><br>
