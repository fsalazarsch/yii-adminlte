<?php
$this->breadcrumbs=array(
	'compras',
);


	$this->menu=array(
	array('label'=>'Crear compra', 'url'=>array('create')),
	array('label'=>'Administrar compras', 'url'=>array('admin')),
	array('label'=>'Exportar a Excel', 'url'=>array('excel')),
	array('label'=>'Importar compras', 'url'=>array('importar')),
);

}
?>

<h1>compras</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
		'filter'=>$model,
	'columns'=>array(
		'id_compra',
		'cantidad_creditos',
		
				
		array(
			'class'=>'CButtonColumn',
			'template'=> '{view}',
		),
	),
)); ?>
<br><br>
