<?php


$gridDataProvider = new CArrayDataProvider(array(
    array('id'=>1, 'firstName'=>'Mark', 'lastName'=>'Otto', 'language'=>'CSS','usage'=>'<span class="inlinebar">1,3,4,5,3,5</span>'),
    array('id'=>2, 'firstName'=>'Jacob', 'lastName'=>'Thornton', 'language'=>'JavaScript','usage'=>'<span class="inlinebar">1,3,16,5,12,5</span>'),
    array('id'=>3, 'firstName'=>'Stu', 'lastName'=>'Dent', 'language'=>'HTML','usage'=>'<span class="inlinebar">1,4,4,7,5,9,10</span>'),
	array('id'=>4, 'firstName'=>'Jacob', 'lastName'=>'Thornton', 'language'=>'JavaScript','usage'=>'<span class="inlinebar">1,3,16,5,12,5</span>'),
    array('id'=>5, 'firstName'=>'Stu', 'lastName'=>'Dent', 'language'=>'HTML','usage'=>'<span class="inlinebar">1,3,4,5,3,5</span>'),
));

/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl; 
?>


  <div class="span12">
  	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>"<i class='icon-user'></i> Perfil Personal",
		));
		
	?>
  		<?php
  		//Yii::app()->db->createCommand('SELECT COUNT(*) FROM vin_servicio WHERE cobro = 0 OR cobro IS NULL')->queryScalar()
  		
Yii::app()->user->setFlash('error', "<strong>Nuevo Servicio Creado</strong><br> (Aqui se ponen los datos del servicio)");

foreach(Yii::app()->user->getFlashes() as $key => $message) {
	echo '<div class="alert alert-' . $key . '">'
	 		//.'<button type="button" class="close" data-dismiss="alert">×</button>'
			. $message . "</div>\n";
}
?>


		<table class="table table-bordered table-condensed">
		<tr><td class="span5">
		<?php $id = User::model()->findByPk(Yii::app()->user->id);
		 //if(!$id->foto)
		 echo '<img src="'.Yii::app()->baseUrl.'/data/user.png" width="200px;" height="200px;" >'; 
		 //else
		 //echo '<img src="'.Yii::app()->baseUrl.'/data/"'.$id->foto.'" width="200px;" height="200px;" >'; 
		 
		 
		 ?>	
		
		</td>
		<td>
		<h3><?php echo Yii::app()->user->name; ?></h3>
		
		<?php echo 'Email: '.$id->email.'<br>'; 
		/*$tel = User::model()->findByPk(Yii::app()->user->id)->telefono;
		 if((!$tel) || ($tel == 0)|| ($tel == ""))
		 
		 echo '<kbd style="color: red;font-size: 20px;"><b> POR FAVOR INGRESE SU TELEFONO</b><br></kbd>';		
		?>
		<?php //echo 'Centrocosto: '.Centrocosto::model()->findByPk(User::model()->findByPk(Yii::app()->user->id)->centrocosto)->nombre.'<br>'; ?>	
		<?php echo '<a href="'.Yii::app()->baseUrl.'/index.php/user/update?id='.Yii::app()->user->id.'">'?> editar</a><br>
		*/?>
		</td></tr>
		</table>
		
	
  </div>
  
  <?php if(Yii::app()->user->isAdmin){?>
<!--div class="row-fluid">
  <div class="span3 ">
	<div class="stat-block">
	  <ul>
		<li class="stat-graph inlinebar" id="weekly-visit">8,4,6,5,9,10</li>
		<li class="stat-count"><span>$23,000</span><span>Weekly Sales</span></li>
		<li class="stat-percent"><span class="text-success stat-percent">20%</span></li>
	  </ul>
	</div>
  </div>
  <div class="span3 ">
	<div class="stat-block">
	  <ul>
		<li class="stat-graph inlinebar" id="new-visits">2,4,9,1,5,7,6</li>
		<li class="stat-count"><span>$123,780</span><span>Monthly Sales</span></li>
		<li class="stat-percent"><span class="text-error stat-percent">-15%</span></li>
	  </ul>
	</div>
  </div>
  <div class="span3 ">
	<div class="stat-block">
	  <ul>
		<li class="stat-graph inlinebar" id="unique-visits">200,300,500,200,300,500,1000</li>
		<li class="stat-count"><span>$12,456</span><span>Open Invoices</span></li>
		<li class="stat-percent"><span class="text-success stat-percent">10%</span></li>
	  </ul>
	</div>
  </div>
  <div class="span3 ">
	<div class="stat-block">
	  <ul>
		<li class="stat-graph inlinebar" id="">1000,3000,6000,8000,3000,8000,10000</li>
		<li class="stat-count"><span>$25,000</span><span>Overdue</span></li>
		<li class="stat-percent"><span><span class="text-success stat-percent">20%</span></li>
	  </ul>
	</div>
  </div>
</div>

<div class="row-fluid">

    
	<div class="span9">
      < ?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'<span class="icon-picture"></span>Operations Chart',
			'titleCssClass'=>''
		));
		?>
        
        <div class="auto-update-chart" style="height: 250px;width:100%;margin-top:15px; margin-bottom:15px;"></div>
        
        < ?php $this->endWidget(); ?>
        
	</div>
	<div class="span3">
		<div class="summary">
          <ul>
          	<li>
          		<span class="summary-icon">
                	<img src="< ?php echo $baseUrl ;?>/img/credit.png" width="36" height="36" alt="Monthly Income">
                </span>
                <span class="summary-number">$78,245</span>
                <span class="summary-title"> Monthly Income</span>
            </li>
            <li>
            	<span class="summary-icon">
                	<img src="< ?php echo $baseUrl ;?>/img/page_white_edit.png" width="36" height="36" alt="Open Invoices">
                </span>
                <span class="summary-number">125</span>
                <span class="summary-title"> Open Invoices</span>
            </li>
            <li>
            	<span class="summary-icon">
                	<img src="< ?php echo $baseUrl ;?>/img/page_white_excel.png" width="36" height="36" alt="Open Quotes<">
                </span>
                <span class="summary-number">53</span>
                <span class="summary-title"> Open Quotes</span>
            </li>
            <li>
            	<span class="summary-icon">
                	<img src="< ?php echo $baseUrl ;?>/img/group.png" width="36" height="36" alt="Active Members">
                </span>
                <span class="summary-number">654,321</span>
                <span class="summary-title"> Active Members</span>
            </li>
            <li>
            	<span class="summary-icon">
                	<img src="< ?php echo $baseUrl ;?>/img/folder_page.png" width="36" height="36" alt="Recent Conversions">
                </span>
                <span class="summary-number">630</span>
                <span class="summary-title"> Recent Conversions</span></li>
        
          </ul>
        </div>

	</div>
</div>


<div class="row-fluid">
	<div class="span6">
	  < ?php $this->widget('zii.widgets.grid.CGridView', array(
			/*'type'=>'striped bordered condensed',*/
			'htmlOptions'=>array('class'=>'table table-striped table-bordered table-condensed'),
			'dataProvider'=>$gridDataProvider,
			'template'=>"{items}",
			'columns'=>array(
				array('name'=>'id', 'header'=>'#'),
				array('name'=>'firstName', 'header'=>'First name'),
				array('name'=>'lastName', 'header'=>'Last name'),
				array('name'=>'language', 'header'=>'Language'),
				array('name'=>'usage', 'header'=>'Usage', 'type'=>'raw'),
				
			),
		)); ?>
	</div><!--/span- ->
	<div class="span6">
		 < ?php $this->widget('zii.widgets.grid.CGridView', array(
			/*'type'=>'striped bordered condensed',*/
			'htmlOptions'=>array('class'=>'table table-striped table-bordered table-condensed'),
			'dataProvider'=>$gridDataProvider,
			'template'=>"{items}",
			'columns'=>array(
				array('name'=>'id', 'header'=>'#'),
				array('name'=>'firstName', 'header'=>'First name'),
				array('name'=>'lastName', 'header'=>'Last name'),
				array('name'=>'language', 'header'=>'Language'),
				array('name'=>'usage', 'header'=>'Usage', 'type'=>'raw'),
				
			),
		)); ?>
        	
	</div><!--/span- ->
</div><!--/row- ->

<div class="row-fluid">
	<div class="span6">
	  < ?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'<span class="icon-th-large"></span>Income Chart',
			'titleCssClass'=>''
		));
		?>
        
        <div class="visitors-chart" style="height: 230px;width:100%;margin-top:15px; margin-bottom:15px;"></div>
        
        < ?php $this->endWidget(); ?>
	</div><!--/span- ->
    <div class="span6">
    	< ?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'<span class="icon-th-list"></span> Visitors Chart',
			'titleCssClass'=>''
		));
		?>
        
        <div class="pieStats" style="height: 230px;width:100%;margin-top:15px; margin-bottom:15px;"></div>
        
        < ?php $this->endWidget(); ?>
    </div>
	<!--<div class="span2">
    	<input class="knob" data-width="100" data-displayInput=false data-fgColor="#5EB95E" value="35">
    </div>
	<div class="span2">
     	<input class="knob" data-width="100" data-cursor=true data-fgColor="#B94A48" data-thickness=.3 value="29">
    </div>
	<div class="span2">
         <input class="knob" data-width="100" data-min="-100" data-fgColor="#F89406" data-displayPrevious=true value="44">     	
	</div><!--/span- ->
</div><!--/row-->
<?php } ?>
  
  
  <?php if(Yii::app()->user->isAdmin){ ?>
	<div class="span12" style="margin-left:0px;">	<br><br>
  	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>"<i class='icon-check'></i> Estado de Servicios",
		));
		
	?><br>
  		<div style="width:100%;margin-top:15px; margin-bottom:15px;"> 
		<span class="badge badge-important"></span><code>Servicios sin valorizar</code><br><br>
		<span class="badge badge-warning"></span><code>Servicios sin operar</code><br><br>
		<span class="badge badge-info"></span><code>Servicios sin hora de termino</code><br><br>
				<span class="badge badge-success"></span><code>Servicios terminados</code><br><br>
		<span class="badge " id="badge"></span><code>Servicios en total</code><br><br>
		</div>
	<?php $this->endWidget();?>
  </div>
<?}?>
  <?php $this->endWidget();?>
	
 
