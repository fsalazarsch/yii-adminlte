<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          
          <div class="nav-collapse">
		  	<img src= "<?php echo $baseUrl?>/img/icons/logo-city.jpg" width="80px">
			<?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
		
						//array('label'=>'Dashboard', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isAdmin),
                        /*array('label'=>'Dashboard', 'url'=>array('/site/index')),*/
                        array('label'=>'Graphs & Charts', 'url'=>array('/site/page', 'view'=>'graphs'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        array('label'=>'Forms', 'url'=>array('/site/page', 'view'=>'forms'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        array('label'=>'Tables', 'url'=>array('/site/page', 'view'=>'tables'), 'visible'=>Yii::app()->user->isSuperAdmin),
						array('label'=>'Interface', 'url'=>array('/site/page', 'view'=>'interface'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        array('label'=>'Typography', 'url'=>array('/site/page', 'view'=>'typography'), 'visible'=>Yii::app()->user->isSuperAdmin),
                        //array('label'=>'Gii generated', 'url'=>array('customer/index')),
                       		array('label'=>'Home', 'url'=>array('/')),
							//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
							//array('label'=>'Contact', 'url'=>array('/site/contact')),
						array('label'=>'Maestros <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                        'items'=>array(
                            array('label'=>'Usuarios', 'url'=>Yii::app()->baseurl.'/user/admin'),
                            array('label'=>'Clientes', 'url'=>Yii::app()->baseurl.'/cliente/admin'),
                            array('label'=>'Tarifas', 'url'=>Yii::app()->baseurl.'/precio'),
							array('label'=>'Motoristas', 'url'=>Yii::app()->baseurl.'/driver/admin'),
							
                        ), 'visible'=>!Yii::app()->user->isGuest),

							array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
							array('label'=>'Servicios', 'url'=>array('/servicio'), 'visible'=>!Yii::app()->user->isGuest),		
							array('label'=>'Planilla Servicios', 'url'=>array('/site/planilla'), 'visible'=>!Yii::app()->user->isGuest),									
								
							array('label'=>'Reportes', 'url'=>array('/site/reporte'), 'visible'=>!Yii::app()->user->isGuest),								
							//array('label'=>'FAQ', 'url'=>array('/site/faq'), 'visible'=>!Yii::app()->user->isGuest),	
							array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        
        	<!--div class="style-switcher pull-left">
                <a href="javascript:chooseStyle('none', 60)" checked="checked"><span class="style" style="background-color:#0088CC;"></span></a>
                <a href="javascript:chooseStyle('style2', 60)"><span class="style" style="background-color:#7c5706;"></span></a>
                <a href="javascript:chooseStyle('style3', 60)"><span class="style" style="background-color:#468847;"></span></a>
                <a href="javascript:chooseStyle('style4', 60)"><span class="style" style="background-color:#4e4e4e;"></span></a>
                <a href="javascript:chooseStyle('style5', 60)"><span class="style" style="background-color:#d85515;"></span></a>
                <a href="javascript:chooseStyle('style6', 60)"><span class="style" style="background-color:#a00a69;"></span></a>
                <a href="javascript:chooseStyle('style7', 60)"><span class="style" style="background-color:#a30c22;"></span></a>
          	</div-->
           <form class="navbar-search pull-right" action="">
           	 
           <!--input type="text" class="search-query span2" placeholder="Buscar"-->
           
           </form>
    	</div><!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->
