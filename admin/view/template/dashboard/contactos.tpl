<div class="tile">  
  <div class="tile-heading"><?php echo $heading_title; ?></div>
  <div class="tile-body">
	<table>
		<tr>
			<td>
  				<i class="fa fa-database"></i><h2 class="pull-right"><?php echo $total_listas; ?></h2>
  			</td>
  		</tr>
  		<tr>
  			<td><i class="fa fa-users"></i>&nbsp;&nbsp;
    			<h2 class="pull-right"><?php echo $total_contactos; ?></h2>
    		</td>
  		</tr>
  	</table>
  	    
  </div>
  <div class="tile-footer">&nbsp;
  <!--<a href="<?php echo $listas; ?>"><?php echo $text_view; ?></a>-->
  </div>
</div>
